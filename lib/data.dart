class LooCafe{
  String name;
  double lat,long;
  double? dist;
  LooCafe({
    required this.name,
    required this.lat,
    required this.long,
    this.dist
});
}

var data = [
  LooCafe(name: " LooCafe DLF Street", lat: 17.447369, long: 78.353196),
  LooCafe(name: " LooCafe General Store", lat: 17.450637, long: 78.363676),
  LooCafe(name: " Loocafe qMART", lat: 17.447135, long: 78.363908),
  LooCafe(name: " Loocafe Automobile", lat: 17.445820, long: 78.363257),
  LooCafe(name: " Loocafe Pan shop", lat: 17.442537, long: 78.371631),
  LooCafe(name: " Loocafe mobile shop", lat: 17.495233, long: 78.322881),
  LooCafe(name: " loocafe mobileshop", lat: 17.495259, long: 78.319931),
  LooCafe(name: " loocafe mobile", lat: 17.495003, long: 78.320401),
  LooCafe(name: "LooCafe KBR Park",lat: 17.42578,long: 78.445168),
  LooCafe(name: "Ntr Statue, Loocafe,Srinagar", lat: 17.433632, long: 78.436878),
  LooCafe(name: "Dr agawal ,Loocafe,panjagutta ", lat: 17.429536, long: 78.450581),
  LooCafe(name: "City center, Loocafe, Banjara Hills ", lat: 17.4161562, long: 78.4485043),
  LooCafe(name: "Siva Cooldrinks ", lat: 17.408329, long: 78.472062),
  LooCafe(name: "Ntr samadhi mini,Loocafe ", lat: 17.411861, long: 78.471188),
  LooCafe(name: "KBR PARK(TEA House)", lat: 17.413874, long: 78.47043),
  LooCafe(name: "NTR Garden, Hanuman Mandir, Loocafe", lat: 17.414174, long: 78.467467),
  LooCafe(name: "Imax opposit,Loocafe", lat: 17.412196, long: 78.465087),
  LooCafe(name: "Free Tea Unit(masabtank)", lat: 17.403735, long: 78.456246),
  LooCafe(name: "Sri Raghavendra Store ", lat: 17.407544, long: 78.459941),
  LooCafe(name: "KHAIRTHABAD post office Loocafe", lat: 17.4067469, long: 78.4623049),
  LooCafe(name: "Food stall", lat: 17.397798, long: 78.454844),
  LooCafe(name: "Free Tea unit(Jubilee Hills)", lat: 17.422439, long: 78.427769),
  LooCafe(name: "KBR PARK (kadak House)", lat: 17.423386, long: 78.424751),
  LooCafe(name: "Mahanya pan shop", lat: 17.425612, long: 78.419874),
  LooCafe(name: "Japanese park unit", lat: 17.423918, long: 78.424348),
  LooCafe(name: "Ratnadeep Opposit unit", lat: 17.417328, long: 78.411086),
  LooCafe(name: "SSV General store ", lat: 17.413956, long: 78.407867),
  LooCafe(name: "Shankar tea stall", lat: 17.410931, long: 78.403306),
  LooCafe(name: "keshv pan shop", lat: 17.410496, long: 78.402719),
  LooCafe(name: "kadak house", lat: 17.3925655, long: 78.4311863),
  LooCafe(name: "Nawabi Chai", lat: 17.397298, long: 78.42008),
  LooCafe(name: "Ranganath Cooldrinks ", lat: 17.441981, long: 78.445847)
];