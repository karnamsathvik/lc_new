import 'package:flutter/material.dart';
import 'package:loocafe_android/new2.dart';
import 'package:loocafe_android/splash.dart';
import 'new.dart';

void main() { runApp(MyApp());}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen(title:'Loo cafe')
    );
  }
}

