import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

class Camera extends StatefulWidget {
  const Camera({Key? key}) : super(key: key);

  @override
  _CameraState createState() => _CameraState();
}



class _CameraState extends State<Camera> {
  late String qrcoderesult;

  Future<void> scanQRCode() async {
    try {
      final qrCode = await FlutterBarcodeScanner.scanBarcode(
        '#ff6666',
        'Cancel',
        true,
        ScanMode.QR,
      );

      if (!mounted) return;
      if(await canLaunch(qrCode)){
        await launch(qrCode);
      }
      else{
        throw 'could not open';
      }

    } on PlatformException {
      print( 'Failed to get platform version.');
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.qr_code,size: 60,),
            Text("Scan to provide your valuable Feedback"),
            ElevatedButton(onPressed: ()async{
              scanQRCode();
            }, child: Text('Scan QR'))
          ],
        ),
      ),
    );
  }
}
